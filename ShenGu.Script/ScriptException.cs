﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace ShenGu.Script
{
    public abstract class ScriptException : Exception
    {
        private ScriptParser parser;
        private int charIndex;
        private int lineIndex, columnIndex;
        
        protected ScriptException(ScriptParser parser, int charIndex, string errorMessage, Exception innerException) : base(errorMessage, innerException)
        {
            this.parser = parser;
            this.charIndex = charIndex;
            this.lineIndex = this.columnIndex = -1;
        }
        protected ScriptException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        private void CheckLine()
        {
            if (parser != null && lineIndex < 0 && charIndex >= 0)
                parser.CheckLine(charIndex, out lineIndex, out columnIndex);
        }

        public ScriptParser Parser { get { return parser; } }
        public string Script { get { return parser != null ? parser.Script : null; } }
        public int CharIndex { get { return charIndex; } }
        public int LineIndex { get { CheckLine(); return lineIndex; } }
        public int ColumnIndex { get { CheckLine(); return columnIndex; } }
    }

    public class ScriptParseException : ScriptException
    {
        internal ScriptParseException(ScriptParser parser, int charIndex, string errorMessage) : base(parser, charIndex, errorMessage, null) { }

        protected ScriptParseException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }

    public interface IScriptException
    {
        IScriptObject Value { get; }
    }

    public class ScriptExecuteException : ScriptException, IScriptException
    {
        private IScriptObject value;

        internal ScriptExecuteException(ScriptParser parser, int charIndex, string message, Exception innerException)
            : base(parser, charIndex, message, innerException) { }
        
        protected ScriptExecuteException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        public IScriptObject Value
        {
            get
            {
                if (value == null)
                    value = ScriptString.Create(this.Message);
                return value;
            }
        }

        private static ScriptExecuteException Create(ScriptContext context, IScriptObject value, string message, Exception innerException)
        {
            ScriptExecuteException result;
            if (context == null || context.CurrentContext == null || context.CurrentContext.Current == null)
                result = new ScriptExecuteException(null, -1, message, innerException);
            else
            {
                ElementBase elem = context.CurrentContext.Current;
                result = new ScriptExecuteException(elem.Parser, elem.CharIndex, message, innerException);
            }
            result.value = value;
            return result;
        }

        public static ScriptExecuteException Create(ScriptContext context, IScriptObject value) { return Create(context, value, null, null); }

        public static ScriptExecuteException Create(ScriptContext context, string message, Exception innerException) { return Create(context, null, message, innerException); }

        public static ScriptExecuteException Create(ScriptContext context, string message) { return Create(context, null, message, null); }

        public static void Throw(ScriptContext context, IScriptObject value) { throw Create(context, value); }

        public static void Throw(ScriptContext context, string message) { throw Create(context, message, null); }

        public static void Throw(ScriptContext context, string message, Exception innerException) { throw Create(context, message, innerException); }
    }
}
