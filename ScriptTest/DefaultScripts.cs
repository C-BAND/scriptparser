﻿using ShenGu.Script;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ScriptTest
{
    public class SimpleScript : IExecutor
    {
        public string ScriptContent { get { return Utils.GetTestContent("Simple.js"); } }

        public Type[] ScriptTypes { get { return null; } }

        public Dictionary<string, object> ScriptValues
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result["status"] = "已完成";
                return result;
            }
        }
    }

    [ScriptMapping("User")]
    public class User
    {
        [ObjectMember]
        public string Name { get; set; }

        [ObjectMember]
        public int Age { get; set; }

        [ObjectMember]
        public string Check(string operate)
        {
            return string.Format("用户（用户名：{0}，年龄：{1}）正在{2}", Name, Age, operate);
        }
    }

    public class TypeMappingExecutor : IExecutor
    {
        public string ScriptContent { get { return Utils.GetTestContent("TypeMapping.js"); } }

        public Type[] ScriptTypes { get { return new Type[] { typeof(User) }; } }

        public Dictionary<string, object> ScriptValues
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result["User"] = typeof(User);
                return result;
            }
        }
    }

    [ScriptProxy(typeof(Form))]
    public class FormProxy : IScriptProxy
    {
        private Form form;

        public object RealInstance
        {
            get { return form; }
            set { form = (Form)value; }
        }

        [ObjectMember]
        public string Title
        {
            get { return form.Text; }
            set { form.Text = value; }
        }

        [ObjectMember]
        public void Show()
        {
            form.Show();
        }
        
        [ObjectMember]
        public void ShowDialog()
        {
            form.ShowDialog();
        }
    }

    public class TypeProxyExecutor : IExecutor
    {
        public string ScriptContent { get { return Utils.GetTestContent("TypeProxy.js"); } }

        public Type[] ScriptTypes
        {
            get
            {
                return new Type[] { typeof(FormProxy) };
            }
        }

        public Dictionary<string, object> ScriptValues
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                result["Form"] = typeof(Form);
                return result;
            }
        }
    }

    public class MethodMappingExecutor : IExecutor
    {
        public string ScriptContent { get { return Utils.GetTestContent("MethodMapping.js"); } }

        [ScriptMapping]
        public DataTable QueryUsers(int type)
        {
            DataTable table = new DataTable("Users");
            table.Columns.Add("UserId", typeof(string));
            table.Columns.Add("UserName", typeof(string));
            table.Columns.Add("Age", typeof(int));
            table.Rows.Add("Admin", "管理员", 25);
            table.Rows.Add("Tester", "测试人员", 24);
            return table;
        }

        public Type[] ScriptTypes
        {
            get
            {
                return null;
            }
        }

        public Dictionary<string, object> ScriptValues
        {
            get
            {
                return null;
            }
        }
    }

    public static class Utils
    {
        private static string rootPath;

        static Utils()
        {
            rootPath = Path.Combine(Environment.CurrentDirectory, "../../html");
        }

        public static string GetBuildPath(string url)
        {
            url = url.Replace('/', '\\');
            return Path.Combine(rootPath, "build", url);
        }

        public static string GetTestPath(string url)
        {
            url = url.Replace('/', '\\');
            return Path.Combine(rootPath, "test", url);
        }

        public static string GetBuildContent(string url)
        {
            string path = GetBuildPath(url);
            using (StreamReader reader = new StreamReader(path))
                return reader.ReadToEnd();
        }

        public static string GetTestContent(string url)
        {
            string path = GetTestPath(url);
            using (StreamReader reader = new StreamReader(path))
                return reader.ReadToEnd();
        }
    }
}
